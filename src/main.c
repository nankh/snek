#include <ncurses.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define DELAY 1000000
#define POINT 100

#define VECTOR_UP (vector) {0, -1}
#define VECTOR_DOWN (vector) {0, 1}
#define VECTOR_LEFT (vector) {-1, 0}
#define VECTOR_RIGHT (vector) {1, 0}

#define HEAD snake[0]
#define TAIL snake[snake_length - 1]

typedef struct { int x, y; } vector;

vector *snake = NULL;

vector velocity;
vector apple;

int snake_length = 10;
int alive = 1;
int score = 0;
int rows = 0;
int cols = 0;

int **grid = NULL;

void free_grid()
{
    for (int i = 0; i < rows; i++)
    {
        free(grid[i]);
    }

    free(grid);
    grid = NULL;
}

void init_grid()
{
    if (grid != NULL)
    {
        free_grid();
    }

    grid = malloc(rows * sizeof(int *));

    for (int i = 0; i < rows; i++)
    {
        grid[i] = malloc(cols * sizeof(int));
    }
}

void free_snake()
{
    free(snake);
    snake = NULL;
}

void init_snake()
{
    if (snake != NULL)
    {
        free_snake();
    }

    snake = malloc(rows * cols * sizeof(vector));

    int x = rand() % cols;
    int y = rand() % rows;

    for (int i = 0; i < snake_length; i++)
    {
        snake[i].x = x;
        snake[i].y = y;
    }

    int direction = rand() % 4;

    switch(direction)
    {
        case 0:
            velocity = VECTOR_UP;
            break;
        case 1:
            velocity = VECTOR_DOWN;
            break;
        case 2:
            velocity = VECTOR_LEFT;
            break;
        case 3:
            velocity = VECTOR_RIGHT;
            break;
    }
}

void init_apple()
{
    while(1)
    {
        int x = rand() % cols;
        int y = rand() % rows;

        if (grid[y][x] == 0)
        {
            apple.x = x;
            apple.y = y;
            break;
        }
    }
}

void init()
{
    initscr();
    noecho();
    curs_set(FALSE);
    nodelay(stdscr, TRUE);

    getmaxyx(stdscr, rows, cols);

    srand(time(NULL));

    init_grid();
    init_snake();
    init_apple();
}

void input()
{
    int key = getch();

    switch (key)
    {
        case 'w': case 'i':
            velocity = velocity.y == 0 ? VECTOR_UP : velocity;
            break;
        case 's': case 'k':
            velocity = velocity.y == 0 ? VECTOR_DOWN : velocity;
            break;
        case 'a': case 'j':
            velocity = velocity.x == 0 ? VECTOR_LEFT : velocity;
            break;
        case 'd': case 'l':
            velocity = velocity.x == 0 ? VECTOR_RIGHT : velocity;
            break;
    }
}

void update()
{
    grid[TAIL.y][TAIL.x] = 0;

    for (int i = snake_length - 1; i > 0; i--)
    {
        snake[i].x = snake[i - 1].x;
        snake[i].y = snake[i - 1].y;

        grid[snake[i].y][snake[i].x] = 1;
    }

    HEAD.x += velocity.x;
    HEAD.y += velocity.y;

    if (HEAD.x < 0 && velocity.x < 0)
    {
        HEAD.x = cols - 1;
    }
    else if (HEAD.x == cols && velocity.x > 0)
    {
        HEAD.x = 0;
    }
    else if (HEAD.y < 0 && velocity.y < 0)
    {
        HEAD.y = rows - 1;
    }
    else if (HEAD.y == rows && velocity.y > 0)
    {
        HEAD.y = 0;
    }

    if (grid[HEAD.y][HEAD.x] == 1)
    {
        alive = 0;
        return;
    }

    if (HEAD.x == apple.x && HEAD.y == apple.y)
    {
        snake_length++;
        score += POINT;

        TAIL.x = HEAD.x;
        TAIL.y = HEAD.y;

        init_apple();
    }
}

void draw()
{
    clear();

    mvprintw(apple.y, apple.x, "@");

    for (int i = 0; i < snake_length; i++)
    {
        mvprintw(snake[i].y, snake[i].x, "O");
    }

    if (alive == 0)
    {
        mvprintw(HEAD.y, HEAD.x, "X");
    }

    refresh();
}

int main(int argc, char **argv)
{
    init();

    while(alive)
    {
        input();
        update();
        draw();
        usleep(DELAY / snake_length);
    }

    free_grid();
    free_snake();

    endwin();

    printf("\nYOU ATE YOURSELF\nYOUR SCORE WAS %d\n\n", score);

    return 0;
}
